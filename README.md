# How to unit test custom validators with `@Autowired` dependencies in Spring Boot

Demo project to show how the validation layer can be unit tested in a Spring Boot project. Special attention is dedicated to how to use Mockito to mock dependencies and validators.

Check out the following blog posts. Each one is dedicated to a specific topic:

- mocking `@Autowired` dependencies in custom constraint validators: https://codemadeclear.com/index.php/2021/01/26/how-to-mock-dependencies-when-unit-testing-custom-validators/
- mocking a custom constraint validator: https://codemadeclear.com/index.php/2021/02/08/how-to-mock-custom-validators-when-unit-testing/
- configuring a custom constraint validator via application properties: https://codemadeclear.com/index.php/2021/03/22/easily-configure-validators-via-properties-in-a-spring-boot-project/
