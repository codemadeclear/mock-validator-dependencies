package com.codemadeclear.mockvalidatordependencies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockValidatorDependenciesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockValidatorDependenciesApplication.class, args);
	}

}
