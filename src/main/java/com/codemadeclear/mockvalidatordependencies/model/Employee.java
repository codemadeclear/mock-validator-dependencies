package com.codemadeclear.mockvalidatordependencies.model;

import com.codemadeclear.mockvalidatordependencies.validation.annotations.ConfigurableSize;
import com.codemadeclear.mockvalidatordependencies.validation.annotations.UniqueSsn;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@UniqueSsn
public class Employee {

    private Long id;
    @NotNull
    @ConfigurableSize(minProperty = "employee.fullName.size.min", maxProperty = "employee.fullName.size.max")
    private String fullName;
    @NotEmpty
    private String socialSecurityNumber;

}
