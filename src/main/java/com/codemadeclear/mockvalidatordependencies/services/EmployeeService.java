package com.codemadeclear.mockvalidatordependencies.services;

import com.codemadeclear.mockvalidatordependencies.model.Employee;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface EmployeeService {

    Employee save(@NotNull @Valid Employee employee);

}
