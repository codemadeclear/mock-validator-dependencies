package com.codemadeclear.mockvalidatordependencies.services;

import com.codemadeclear.mockvalidatordependencies.model.Employee;

public interface EmployeeValidationService {

    boolean isSsnTaken(Employee employee);

}
