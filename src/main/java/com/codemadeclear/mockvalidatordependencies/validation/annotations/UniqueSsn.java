package com.codemadeclear.mockvalidatordependencies.validation.annotations;

import com.codemadeclear.mockvalidatordependencies.validation.validators.UniqueSsnValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = {UniqueSsnValidator.class})
public @interface UniqueSsn {

    String message() default "ssn already taken";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
