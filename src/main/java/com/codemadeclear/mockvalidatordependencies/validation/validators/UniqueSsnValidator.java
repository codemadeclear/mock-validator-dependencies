package com.codemadeclear.mockvalidatordependencies.validation.validators;

import com.codemadeclear.mockvalidatordependencies.model.Employee;
import com.codemadeclear.mockvalidatordependencies.services.EmployeeValidationService;
import com.codemadeclear.mockvalidatordependencies.validation.annotations.UniqueSsn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class UniqueSsnValidator implements ConstraintValidator<UniqueSsn, Employee> {

    private final EmployeeValidationService employeeValidationService;

    @Autowired
    public UniqueSsnValidator(EmployeeValidationService employeeValidationService) {
        this.employeeValidationService = employeeValidationService;
    }

    @Override
    public boolean isValid(Employee value, ConstraintValidatorContext context) {
        log.debug("Inside the custom constraint validator...");
        return !employeeValidationService.isSsnTaken(value);
    }

}
