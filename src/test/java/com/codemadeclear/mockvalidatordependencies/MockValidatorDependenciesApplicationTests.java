package com.codemadeclear.mockvalidatordependencies;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MockValidatorDependenciesApplicationTests {

	@Autowired
	private Validator validator;
	@Value("${employee.fullName.size.min}")
	private int fullNameMinSize;
	@Value("${employee.fullName.size.max}")
	private int fullNameMaxSize;

	@Test
	void contextLoads() {
	}

	@Test
	void validatorIsOfExpectedType() {
		assertEquals(LocalValidatorFactoryBean.class, validator.getClass());
	}

	@Test
	void fullNameMinSizeHasExpectedValue() {
		assertEquals(3, fullNameMinSize);
	}

	@Test
	void fullNameMaxSizeHasExpectedValue() {
		assertEquals(128, fullNameMaxSize);
	}

}
