package com.codemadeclear.mockvalidatordependencies.services;

import com.codemadeclear.mockvalidatordependencies.CustomLocalValidatorFactoryBean;
import com.codemadeclear.mockvalidatordependencies.model.Employee;
import com.codemadeclear.mockvalidatordependencies.validation.annotations.ConfigurableSize;
import com.codemadeclear.mockvalidatordependencies.validation.annotations.UniqueSsn;
import com.codemadeclear.mockvalidatordependencies.validation.validators.ConfigurableSizeCharSequenceValidator;
import com.codemadeclear.mockvalidatordependencies.validation.validators.UniqueSsnValidator;
import lombok.SneakyThrows;
import org.hibernate.validator.internal.engine.ValidatorImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.PropertyResolver;

import javax.validation.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class EmployeeServiceValidationTest {

    private final static Employee VALID_EMPLOYEE = new Employee();
    private final static Employee BAD_EMPLOYEE_NULL_FULL_NAME = new Employee();
    private final static Employee BAD_EMPLOYEE_EMPTY_FULL_NAME = new Employee();
    private final static Employee BAD_EMPLOYEE_FULL_NAME_TOO_SHORT = new Employee();
    private final static Employee BAD_EMPLOYEE_FULL_NAME_TOO_LONG = new Employee();
    private final static Employee BAD_EMPLOYEE_NULL_SSN = new Employee();
    private final static Employee BAD_EMPLOYEE_EMPTY_SSN = new Employee();
    private static final String VALID_FULL_NAME = "John Doe";
    private static final String VALID_SSN = "1234";

    private static void initValidEmployee() {
        VALID_EMPLOYEE.setId(null).setFullName(VALID_FULL_NAME).setSocialSecurityNumber(VALID_SSN);
    }

    private static void initBadEmployeeNullFullName() {
        BAD_EMPLOYEE_NULL_FULL_NAME.setId(null).setFullName(null).setSocialSecurityNumber(VALID_SSN);
    }

    private static void initBadEmployeeEmptyFullName() {
        BAD_EMPLOYEE_EMPTY_FULL_NAME.setId(null).setFullName("").setSocialSecurityNumber(VALID_SSN);
    }

    private static void initBadEmployeeFullNameTooShort() {
        BAD_EMPLOYEE_FULL_NAME_TOO_SHORT.setId(null).setFullName("Jo").setSocialSecurityNumber(VALID_SSN);
    }

    private static void initBadEmployeeFullNameTooLong() {
        BAD_EMPLOYEE_FULL_NAME_TOO_LONG.setId(null)
                .setFullName("Daenerys Stormborn of the House Targaryen, First of Her Name, the Unburnt, Queen of " +
                        "the Andals and the First Men, Khaleesi of the Great Grass Sea, Breaker of Chains, " +
                        "and Mother of Dragons")
                .setSocialSecurityNumber(VALID_SSN);
    }

    private static void initBadEmployeeNullSsn() {
        BAD_EMPLOYEE_NULL_SSN.setId(null).setFullName(VALID_FULL_NAME).setSocialSecurityNumber(null);
    }

    private static void initBadEmployeeEmptySsn() {
        BAD_EMPLOYEE_EMPTY_SSN.setId(null).setFullName(VALID_FULL_NAME).setSocialSecurityNumber("");
    }

    private static void initEmployees() {
        initValidEmployee();
        initBadEmployeeNullFullName();
        initBadEmployeeEmptyFullName();
        initBadEmployeeFullNameTooShort();
        initBadEmployeeFullNameTooLong();
        initBadEmployeeNullSsn();
        initBadEmployeeEmptySsn();
    }

    private final static AtomicLong EMPLOYEE_SEQ = new AtomicLong(1);

    private static long getNextEmployeeSequence() {
        return EMPLOYEE_SEQ.getAndIncrement();
    }

    private static void resetEmployeeSequence() {
        EMPLOYEE_SEQ.set(1);
    }

    private final EmployeeValidationService mockEmployeeValidationService = mock(EmployeeValidationService.class);
    private final PropertyResolver mockPropertyResolver = mock(PropertyResolver.class);
    private final List<ConstraintValidator<?, ?>> customConstraintValidators =
            Arrays.asList(
                    new UniqueSsnValidator(mockEmployeeValidationService),
                    new ConfigurableSizeCharSequenceValidator(mockPropertyResolver));
    private final ValidatorFactory customValidatorFactory = new CustomLocalValidatorFactoryBean(customConstraintValidators);
    private final Validator validator = customValidatorFactory.getValidator();
    private final EmployeeService mockEmployeeService = mock(EmployeeService.class);
    private final EmployeeService validationProxy = new EmployeeService() {
        @SneakyThrows
        @Override
        public Employee save(@NotNull @Valid Employee employee) {
            validateArg(
                    mockEmployeeService,
                    mockEmployeeService.getClass().getMethod("save", Employee.class),
                    new Object[]{employee});
            return mockEmployeeService.save(employee);
        }

        private void validateArg(EmployeeService service, Method method, Object[] args) {
            Set<ConstraintViolation<EmployeeService>> constraintViolations =
                    validator.forExecutables().validateParameters(service, method, args);
            if (!constraintViolations.isEmpty()) throw new ConstraintViolationException(constraintViolations);
        }
    };

    void setupMocks() {
        when(mockEmployeeService.save(isA(Employee.class))).then(invocation -> {
            Employee argEmployee = invocation.getArgument(0, Employee.class);
            Long argEmployeeId = argEmployee.getId();
            Long id = argEmployeeId != null ? argEmployeeId : getNextEmployeeSequence();
            return argEmployee.setId(id);
        });
        isSsnTakenWillReturn(false);
        fullNameSizeMustBeBetween(3, 128);
    }

    private void isSsnTakenWillReturn(boolean b) {
        when(mockEmployeeValidationService.isSsnTaken(isA(Employee.class))).thenReturn(b);
    }

    @SuppressWarnings("SameParameterValue")
    private void fullNameSizeMustBeBetween(int min, int max) {
        when(mockPropertyResolver.getRequiredProperty(eq("employee.fullName.size.min"), eq(Integer.class)))
                .thenReturn(min);
        when(mockPropertyResolver.getRequiredProperty(eq("employee.fullName.size.max"), eq(Integer.class)))
                .thenReturn(max);
    }

    @BeforeEach
    void setupTest() {
        resetEmployeeSequence();
        initEmployees();
        setupMocks();
    }

    @Test
    void validatorIsOfExpectedType() {
        assertEquals(ValidatorImpl.class, validator.getClass());
    }

    @Test
    void givenValidEmployeeWhenSaveThenOk() {
        Employee employee = assertDoesNotThrow(() -> validationProxy.save(VALID_EMPLOYEE));
        assertNotNull(employee.getId());
    }

    @Test
    void givenValidEmployeeButSsnIsTakenWhenSaveThenCve() {
        isSsnTakenWillReturn(true);
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(VALID_EMPLOYEE));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(UniqueSsn.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    void givenNullEmployeeWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(null));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(NotNull.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    private ConstraintViolation<?> getFirstConstraintViolation(Set<ConstraintViolation<?>> violations) {
        return violations.iterator().next();
    }

    @Test
    void givenEmployeeWithNullFullNameWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(BAD_EMPLOYEE_NULL_FULL_NAME));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(NotNull.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    void givenEmployeeWithEmptyFullNameWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(BAD_EMPLOYEE_EMPTY_FULL_NAME));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(ConfigurableSize.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    void givenEmployeeWithFullNameTooShortWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(BAD_EMPLOYEE_FULL_NAME_TOO_SHORT));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(ConfigurableSize.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    void givenEmployeeWithFullNameTooLongWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(BAD_EMPLOYEE_FULL_NAME_TOO_LONG));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(ConfigurableSize.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    void givenEmployeeWithNullSsnWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(BAD_EMPLOYEE_NULL_SSN));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(NotEmpty.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    void givenEmployeeWithEmptySsnWhenSaveThenCve() {
        ConstraintViolationException cve = assertThrows(ConstraintViolationException.class,
                () -> validationProxy.save(BAD_EMPLOYEE_EMPTY_SSN));
        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        assertEquals(1, violations.size());
        assertEquals(NotEmpty.class,
                getFirstConstraintViolation(violations).getConstraintDescriptor().getAnnotation().annotationType());
    }

}
